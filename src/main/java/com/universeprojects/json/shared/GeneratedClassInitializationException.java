package com.universeprojects.json.shared;


@SuppressWarnings("unused")
public class GeneratedClassInitializationException extends RuntimeException {
    public GeneratedClassInitializationException() {
    }

    public GeneratedClassInitializationException(String message) {
        super(message);
    }

    public GeneratedClassInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneratedClassInitializationException(Throwable cause) {
        super(cause);
    }
}
